# File Organizer

This project organizes files to a designated employee folder and create subfolders inside each employee folder. It reads only the filename of a file. Based on the filename it will:

- identify the employee
- create a folder for that employee
- identify where sub folder it needs to be moved based on keywords
- move the file to it's designated folder

> This application doesn't read the contents of a file. It only reads the filename and uses that to classify the file.

## Getting Started

### Source Code and Reference Files

In this repository you will find multiple files that you will need to run the File Organizer application:

- [PortableJava.zip](./PortalableJava.zip): FileOrganizer.jar needs Java to run. Normally Java is a coffee, coffee makes us work better, right? ^_^  Same thing with the File Organizer application. It needs its coffee. We made an instant coffee, packaged into a zip. You will use this to run the File Organizer application.
- [FileOrganizer.jar](./FileOrganizer.jar): This is the application itself that does the magic (hopefully).
- [keywords.csv](./keywords.csv): This is being read by the FileOrganizer.jar while it does it's magic. This identifes how a file should be moved to subfolders. More description in the Keywords section below.
- [employees.csv](./): We don't keep this file and it's confidential. So you need to create this manually on your laptop.

#### PortableJava.zip

Ideally Java should be installed on your machine. Once installed, in the Windows Command Prompt, you can type in "java" and it will produce multiple options on how to run Java applications. But we work on companies that doesn't allow us to install anything on our machines, really it's a pain in the neck sometimes ^_^. 

PortableJava is, well as the name implies, a portable version of Java. No need to be installed on your laptop, it's safe and does not need admin rights to run.

Unzip the file and all you need to do is access this folder inside PortableJava:

`PortableJava\Java\bin\java.exe`

> **TLDR;** Just extract PortableJava.zip. You will need it to run FileOrganizer.jar.

#### employees.csv

An employee Comma Separated Value (CSV) file is a reference file of all the employees. 

For safety measures, the application doesn't keep this file. So you need to create this on your own, on your local machine. It only needs to have a content with this structure:

`Employee ID, Last Name, First Name`

All employees need to be in this file. If you already have this information in an Excel file, you can convert it to a CSV file by clicking File > Save As > Filename: employees.csv, File Format: CSV. Just make sure you only have the Employee ID, Last Name and First Name columns in the same order specified above.

> **TLDR;** employees.csv contains id, last name and first name of all employees. Tip: just get a copy from MJ :))

#### FileOrganizer.jar

The main application that creates employee folders and subfolders and moves files accordingly.


#### keywords.csv

This file is used by FileOrganizer.jar to identify where to move files to sub folders. The content of this file has this structure:

`Folder A, Sub Folder A1, Keyword`

Example:

`Starting Documents,Amendments,Change of details`

This means, FileOrganizer.jar will create a folder `Starting Documents`. Under `Starting Documents` a subfolder `Amendments`.

If a filename contains a text `Change of details`, then that file will be moved to `Starting Documents\Amendments`.

In [keywords.csv](./keywords.csv), we have already identified the predefined folders and keywords. However feel free to edit the file and add new lines for new folders or new keywords.

Limitation: FileOrganizer.jar can only handle maximum of 2 folders.

> **TLDR;** keywords.csv defines the subfolders and filename keywords for that subfolder. You can add more folders-keyword grouping, the code will automatically cover them.

## CHEAT SHEET

Follow this folder structure:
```
C:\Users\<your_username>  
│
└─── PortableJava\Java
│   
└─── FilingTools
│   │   FileOrganizer.jar
│   │   employees.csv
│   │   keywords.csv
│   
└───Filing
    │   12345 John Smith Offboarding.pdf
    │   ... <all other files that needs to be sorted>
```

Inside `C:\Users\<your_username>`, extract PortableJava.zip. Create a folder `FilingTools`. Put `FileOrganizer.jar`, `employees.csv` and `keywords.csv` inside `FilingTools`. Create a folder `Filing`, and put here all the files you want to organize.

> `<your_username>`  is your user profile in your laptop. 

> `PortableJava` folder is the extracted zip file. Important is it has `Java\bin\java.exe` file inside it

> `<all other files that needs to be sorted>` Other files that you wants to be sorted / moved / organized.


Once you have the same folder structure and files above, open Command Prompt and execute these commands (after each command hit Enter):

```
cd C:\Users\<your_username>
```

```
PortableJava\Java\bin\java.exe -jar FilingTools\FileOrganizer.jar Filing\ 
```

It will show information about files that weren't move, if any. Possible reasons are:

- A file is linked to multiple employee, either because
    - an employee is not listed in employees.csv, or
    - filename of the file is not descriptive enough to identify a single user
- No employee identification can be extracted from the filename

By the end of the execution it will show:

```
=========================
That's it! Did it work?
=========================
```

which means, it is already done and it's time for you to verify the Filing folder ^_^.

Go to `C:\Users\<your_username>\Filing` in your Windows Explorer and the new folders should be created. All remaining files are those that were skipped as the application weren't able to match an employee to it.


